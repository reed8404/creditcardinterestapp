package creditCardInterestApp.app.utility;

/**
 * Created by ReedW on 12/20/2015.
 */
public interface Calculator {

    public double getSimpleInterest();
    public void calculateSimpleInterest();

    public double getNewBalance();
    public void calculateNewBalance();

    public double getOriginalBalance();
    public void calculateOriginalBalance();

    void runCalculations();

}
