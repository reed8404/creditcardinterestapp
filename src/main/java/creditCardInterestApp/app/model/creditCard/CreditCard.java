package creditCardInterestApp.app.model.creditCard;


import creditCardInterestApp.app.model.AccountEntity;
import creditCardInterestApp.app.utility.Calculator;

/**
 * Created by ReedW on 12/18/2015.
 */
public abstract class CreditCard extends AccountEntity implements Calculator {
    protected static final double TIME_PERIOD = 1.0;
    protected double interestRate;

    @Override
    public void calculateOriginalBalance() {
        originalBalance = originalBalance;
    }

    @Override
    public void calculateSimpleInterest() {
        simpleInterest = originalBalance * interestRate * TIME_PERIOD;
    }

    @Override
    public void calculateNewBalance() {
        newBalance = simpleInterest + originalBalance;
    }
}
