package creditCardInterestApp.app.model.creditCard.brand;

import creditCardInterestApp.app.model.creditCard.CreditCard;
import creditCardInterestApp.app.utility.Calculator;

/**
 * Created by ReedW on 12/18/2015.
 */
public class MasterCard extends CreditCard implements Calculator {

    public MasterCard() {
        super();
        this.originalBalance = 100.0;
        this.interestRate = 0.05;
    }
}
