package creditCardInterestApp.app.model.wallet;

import creditCardInterestApp.app.model.AccountEntity;
import creditCardInterestApp.app.model.creditCard.CreditCard;
import creditCardInterestApp.app.utility.Calculator;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by ReedW on 12/18/2015.
 */
public class Wallet extends AccountEntity implements Calculator {

    public Wallet(CreditCard...creditCards) {
        super();
        this.accountEntities = new HashSet<>();
        Collections.addAll(this.accountEntities, creditCards);
    }
}
