package creditCardInterestApp.app.model;

import creditCardInterestApp.app.utility.Calculator;

import java.util.Set;

/**
 * Created by ReedW on 12/23/2015.
 */
public abstract class AccountEntity implements Calculator {
    protected Set<AccountEntity> accountEntities;
    protected double originalBalance;
    protected double simpleInterest;
    protected double newBalance;

    @Override
    public void runCalculations() {
        calculateOriginalBalance();
        calculateSimpleInterest();
        calculateNewBalance();
    }

    @Override
    public double getOriginalBalance() {
        return originalBalance;
    }

    @Override
    public void calculateOriginalBalance() {
        double tempOriginalBalance = 0.0;
        for(AccountEntity accountEntity : accountEntities) {
            tempOriginalBalance += accountEntity.getOriginalBalance();
        }
        originalBalance = tempOriginalBalance;
    }

    @Override
    public double getSimpleInterest() {
        return simpleInterest;
    }

    @Override
    public void calculateSimpleInterest() {
        double tempSimpleInterest = 0.0;
        for (AccountEntity accountEntity : accountEntities) {
            tempSimpleInterest += accountEntity.getSimpleInterest();
        }
        simpleInterest = tempSimpleInterest;
    }

    @Override
    public double getNewBalance() {
        return newBalance;
    }

    @Override
    public void calculateNewBalance() {
        double tempOriginalBalance = getOriginalBalance();
        double tempSimpleInterest = getSimpleInterest();
        newBalance = tempOriginalBalance + tempSimpleInterest;
    }
}
