package creditCardInterestApp.app.model.account;

import creditCardInterestApp.app.model.AccountEntity;
import creditCardInterestApp.app.model.person.Person;
import creditCardInterestApp.app.utility.Calculator;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by ReedW on 12/21/2015.
 */
public class Account extends AccountEntity implements Calculator {

    public Account(Person...persons) {
        super();
        this.accountEntities = new HashSet<>();
        Collections.addAll(this.accountEntities, persons);
    }
}
