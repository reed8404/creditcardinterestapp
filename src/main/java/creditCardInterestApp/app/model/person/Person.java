package creditCardInterestApp.app.model.person;

import creditCardInterestApp.app.model.AccountEntity;
import creditCardInterestApp.app.model.wallet.Wallet;
import creditCardInterestApp.app.utility.Calculator;

import java.util.Collections;
import java.util.HashSet;

/**
 * Created by ReedW on 12/18/2015.
 */
public class Person extends AccountEntity implements Calculator {

    public Person(Wallet...wallets) {
        super();
        this.accountEntities = new HashSet<>();
        Collections.addAll(this.accountEntities, wallets);
    }
}
