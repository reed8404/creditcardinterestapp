package creditCardInterestApp.demo.demoCase;

import creditCardInterestApp.app.model.account.Account;
import creditCardInterestApp.app.model.creditCard.CreditCard;
import creditCardInterestApp.app.model.person.Person;
import creditCardInterestApp.app.model.wallet.Wallet;

/**
 * Created by ReedW on 12/21/2015.
 */
public abstract class Case {

    protected CreditCard visa;
    protected CreditCard masterCard;
    protected CreditCard discover;
    protected Wallet wallet;
    protected Wallet walletTwo;
    protected Person person;
    protected Person personTwo;
    protected Account account;

    protected double visaOriginalBalance;
    protected double visaSimpleInterest;
    protected double visaNewBalance;

    protected double masterCardOriginalBalance;
    protected double masterCardSimpleInterest;
    protected double masterCardNewBalance;

    protected double discoverOriginalBalance;
    protected double discoverSimpleInterest;
    protected double discoverNewBalance;

    protected double walletOriginalBalance;
    protected double walletSimpleInterest;
    protected double walletNewBalance;

    protected double walletTwoOriginalBalance;
    protected double walletTwoSimpleInterest;
    protected double walletTwoNewBalance;

    protected double personOriginalBalance;
    protected double personSimpleInterest;
    protected double personNewBalance;

    protected double personTwoOriginalBalance;
    protected double personTwoSimpleInterest;
    protected double personTwoNewBalance;

    protected double accountOriginalBalance;
    protected double accountSimpleInterest;
    protected double accountNewBalance;

    public void setVisa(){
        visa.runCalculations();
        visaOriginalBalance = visa.getOriginalBalance();
        visaSimpleInterest = visa.getSimpleInterest();
        visaNewBalance =  visa.getNewBalance();
    }

    public void setMasterCard() {
        masterCard.runCalculations();
        masterCardOriginalBalance = masterCard.getOriginalBalance();
        masterCardSimpleInterest = masterCard.getSimpleInterest();
        masterCardNewBalance = masterCard.getNewBalance();
    }

    public void setDiscover() {
        discover.runCalculations();
        discoverOriginalBalance = discover.getOriginalBalance();
        discoverSimpleInterest = discover.getSimpleInterest();
        discoverNewBalance = discover.getNewBalance();
    }

    public void setWallet() {
        wallet.runCalculations();
        walletOriginalBalance = wallet.getOriginalBalance();
        walletSimpleInterest = wallet.getSimpleInterest();
        walletNewBalance = wallet.getNewBalance();
    }

    public void setWalletTwo() {
        walletTwo.runCalculations();
        walletTwoOriginalBalance = walletTwo.getOriginalBalance();
        walletTwoSimpleInterest = walletTwo.getSimpleInterest();
        walletTwoNewBalance = walletTwo.getNewBalance();
    }

    public void setPerson(){
        person.runCalculations();
        personOriginalBalance = person.getOriginalBalance();
        personSimpleInterest = person.getSimpleInterest();
        personNewBalance = person.getNewBalance();
    }

    public void setPersonTwo() {
        personTwo.runCalculations();
        personTwoOriginalBalance = personTwo.getOriginalBalance();
        personTwoSimpleInterest = personTwo.getSimpleInterest();
        personTwoNewBalance = personTwo.getNewBalance();
    }

    public void setAccount(){
        account.runCalculations();
        accountOriginalBalance = account.getOriginalBalance();
        accountSimpleInterest = account.getSimpleInterest();
        accountNewBalance = account.getNewBalance();
    }
    public abstract void displayCaseSpecs();
    public abstract void displayCaseCalculations();

}
