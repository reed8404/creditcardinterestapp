package creditCardInterestApp.demo.demoCase.demoCaseImpl;

import creditCardInterestApp.app.model.account.Account;
import creditCardInterestApp.app.model.creditCard.brand.MasterCard;
import creditCardInterestApp.app.model.creditCard.brand.Visa;
import creditCardInterestApp.app.model.person.Person;
import creditCardInterestApp.app.model.wallet.Wallet;
import creditCardInterestApp.demo.demoCase.Case;

/**
 * Created by ReedW on 12/21/2015.
 */
public class CaseThree extends Case {

    public CaseThree() {
        super();
        this.visa = new Visa();
        setVisa();
        this.masterCard = new MasterCard();
        setMasterCard();
        this.wallet = new Wallet(visa, masterCard);
        setWallet();
        this.person = new Person(wallet);
        setPerson();
        this.personTwo = new Person(wallet);
        setPersonTwo();
        this.account = new Account(person, personTwo);
        setAccount();
    }

    @Override
    public void displayCaseSpecs() {
        System.out.println("Test Case Three:\n\n" +
                        "This account has 2 people.\n" +
                        "Person 1 has 1 wallets.\n" +
                        "This wallet has 2 cards: 1 Visa at 10% interest, 1 Master Card at 5% interest.\n" +
                        "Person 2 has 1 wallet.\n" +
                        "This wallet also has 2 cards: 1 Visa at 10% interest, 1 Master Card at 5% interest.\n" +
                        "Each card has a $100 balance.\n" +
                        "Calculate the: original balance, total interest(simple interest), and new balance including interest.\n" +
                        "For: this account, its people, their wallets, and the cards inside of each wallet.\n\n"
        );
    }

    @Override
    public void displayCaseCalculations() {
        System.out.println("Calculations:\n\n" +
                        "The original balance on this account:\n" +
                        "Expected: 400.0\n" +
                        "Actual: " + accountOriginalBalance + "\n\n" +
                        "The simple interest on this account:\n" +
                        "Expected: 30.0\n" +
                        "Actual: " + accountSimpleInterest + "\n\n" +
                        "The new balance of this account:\n" +
                        "Expected: 430.0\n" +
                        "Actual: " + accountNewBalance + "\n\n" +
                        "The original balance for the first person:\n" +
                        "Expected: 200.0\n" +
                        "Actual: " + personOriginalBalance + "\n\n" +
                        "The simple interest for the first person:\n" +
                        "Expected: 15.0\n" +
                        "Actual: " + personSimpleInterest + "\n\n" +
                        "The new balance for the first person:\n" +
                        "Expected: 215.0\n" +
                        "Actual: " + personNewBalance + "\n\n" +
                        "The original balance for the first person's wallet:\n" +
                        "Expected: 200.0\n" +
                        "Actual: " + walletOriginalBalance + "\n\n" +
                        "The simple interest for the first persons's wallet:\n" +
                        "Expected: 15.0\n" +
                        "Actual: " + walletSimpleInterest + "\n\n" +
                        "The new balance for the first person's wallet:\n" +
                        "Expected: 215.0\n" +
                        "Actual: " + walletNewBalance + "\n\n" +
                        "The original balance for the visa in the first wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + visaOriginalBalance + "\n\n" +
                        "The simple interest for the visa in the first wallet:\n" +
                        "Expected: 10.0\n" +
                        "Actual: " + visaSimpleInterest + "\n\n" +
                        "The new balance for the visa in the first wallet:\n" +
                        "Expected: 110.0\n" +
                        "Actual: " + visaNewBalance + "\n\n" +
                        "The original balance for the Master Card in the first wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + masterCardOriginalBalance + "\n\n" +
                        "The simple interest for the Master Card in the first wallet:\n" +
                        "Expected: 5.0\n" +
                        "Actual: " + masterCardSimpleInterest + "\n\n" +
                        "The new balance for the Master Card in the first wallet:\n" +
                        "Expected: 105.0\n" +
                        "Actual: " + masterCardNewBalance + "\n\n" +
                        "The original balance for the second person:\n" +
                        "Expected: 200.0\n" +
                        "Actual: " + personOriginalBalance + "\n\n" +
                        "The simple interest for the second person:\n" +
                        "Expected: 15.0\n" +
                        "Actual: " + personSimpleInterest + "\n\n" +
                        "The new balance for the second person:\n" +
                        "Expected: 215.0\n" +
                        "Actual: " + personNewBalance + "\n\n" +
                        "The original balance for the second person's wallet:\n" +
                        "Expected: 200.0\n" +
                        "Actual: " + walletOriginalBalance + "\n\n" +
                        "The simple interest for the second person's wallet:\n" +
                        "Expected: 15.0\n" +
                        "Actual: " + walletSimpleInterest + "\n\n" +
                        "The new balance for the second person's wallet:\n" +
                        "Expected: 215.0\n" +
                        "Actual: " + walletNewBalance + "\n\n" +
                        "The original balance for the visa in the second wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + visaOriginalBalance + "\n\n" +
                        "The simple interest for the visa in the second wallet:\n" +
                        "Expected: 10.0\n" +
                        "Actual: " + visaSimpleInterest + "\n\n" +
                        "The new balance for the visa in the second wallet:\n" +
                        "Expected: 110.0\n" +
                        "Actual: " + visaNewBalance + "\n\n" +
                        "The original balance for the Master Card in the second wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + masterCardOriginalBalance + "\n\n" +
                        "The simple interest for the Master Card in the second wallet:\n" +
                        "Expected: 5.0\n" +
                        "Actual: " + masterCardSimpleInterest + "\n\n" +
                        "The new balance for the Master Card in the second wallet:\n" +
                        "Expected: 105.0\n" +
                        "Actual: " + masterCardNewBalance + "\n\n\n\n"
        );
    }
}
