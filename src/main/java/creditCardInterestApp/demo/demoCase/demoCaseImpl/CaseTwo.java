package creditCardInterestApp.demo.demoCase.demoCaseImpl;

import creditCardInterestApp.app.model.account.Account;
import creditCardInterestApp.app.model.creditCard.brand.Discover;
import creditCardInterestApp.app.model.creditCard.brand.MasterCard;
import creditCardInterestApp.app.model.creditCard.brand.Visa;
import creditCardInterestApp.app.model.person.Person;
import creditCardInterestApp.app.model.wallet.Wallet;
import creditCardInterestApp.demo.demoCase.Case;

/**
 * Created by ReedW on 12/21/2015.
 */
public class CaseTwo extends Case {

    public CaseTwo() {
        super();
        this.visa = new Visa();
        setVisa();
        this.masterCard = new MasterCard();
        setMasterCard();
        this.discover = new Discover();
        setDiscover();
        this.wallet = new Wallet(visa, discover);
        setWallet();
        this.walletTwo = new Wallet(masterCard);
        setWalletTwo();
        this.person = new Person(wallet, walletTwo);
        setPerson();
        this.account = new Account(person);
        setAccount();
    }

    @Override
    public void displayCaseSpecs() {
        System.out.println("Test Case Two:\n\n" +
                "This account has 1 person.\n" +
                "This person has 2 wallets.\n" +
                "Wallet 1 has 2 cards: 1 Visa at 10% interest, 1 Discover at 1% interest.\n" +
                "Wallet 2 has 1 card: 1 Master Card at 5% interest.\n" +
                "Each card has a $100 balance.\n" +
                "Calculate the: original balance, total interest(simple interest), and new balance including interest.\n" +
                "For: This account, its people, their wallets, and the cards inside of each wallet.\n\n"
        );
    }

    @Override
    public void displayCaseCalculations() {
        System.out.println("Calculations:\n\n" +
                        "The original balance on this account:\n" +
                        "Expected: 300.0\n" +
                        "Actual: " + accountOriginalBalance + "\n\n" +
                        "The simple interest on this account:\n" +
                        "Expected: 16.0\n" +
                        "Actual: " + accountSimpleInterest + "\n\n" +
                        "The new balance of this account:\n" +
                        "Expected: 316.0\n" +
                        "Actual: " + accountNewBalance + "\n\n" +
                        "The original balance for this person:\n" +
                        "Expected: 300.0\n" +
                        "Actual: " + personOriginalBalance + "\n\n" +
                        "The simple interest for this person:\n" +
                        "Expected: 16.0\n" +
                        "Actual: " + personSimpleInterest + "\n\n" +
                        "The new balance for this person:\n" +
                        "Expected: 316.0\n" +
                        "Actual: " + personNewBalance + "\n\n" +
                        "The original balance for the first wallet:\n" +
                        "Expected: 200.0\n" +
                        "Actual: " + walletOriginalBalance + "\n\n" +
                        "The simple interest for the first wallet:\n" +
                        "Expected: 11.0\n" +
                        "Actual: " + walletSimpleInterest + "\n\n" +
                        "The new balance for the first wallet:\n" +
                        "Expected: 211.0\n" +
                        "Actual: " + walletNewBalance + "\n\n" +
                        "The original balance for the visa in the first wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + visaOriginalBalance + "\n\n" +
                        "The simple interest for the visa in the first wallet:\n" +
                        "Expected: 10.0\n" +
                        "Actual: " + visaSimpleInterest + "\n\n" +
                        "The new balance for the visa in the first wallet:\n" +
                        "Expected: 110.0\n" +
                        "Actual: " + visaNewBalance + "\n\n" +
                        "The original balance for the Discover in the first wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + discoverOriginalBalance + "\n\n" +
                        "The simple interest for the Discover in the first wallet:\n" +
                        "Expected: 1.0\n" +
                        "Actual: " + discoverSimpleInterest + "\n\n" +
                        "The new balance for the Discover in the first wallet:\n" +
                        "Expected: 101.0\n" +
                        "Actual: " + discoverNewBalance + "\n\n" +
                        "The original balance for the second wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + walletTwoOriginalBalance + "\n\n" +
                        "The simple interest for the second wallet:\n" +
                        "Expected: 5.0\n" +
                        "Actual: " + walletTwoSimpleInterest + "\n\n" +
                        "The new balance for the second wallet:\n" +
                        "Expected: 105.0\n" +
                        "Actual: " + walletTwoNewBalance + "\n\n" +
                        "The original balance for the Master Card in the second wallet:\n" +
                        "Expected: 100.0\n" +
                        "Actual: " + masterCardOriginalBalance + "\n\n" +
                        "The simple interest for the Master Card in the second wallet:\n" +
                        "Expected: 5.0\n" +
                        "Actual: " + masterCardSimpleInterest + "\n\n" +
                        "The new balance for the Master Card in the second wallet:\n" +
                        "Expected: 105.0\n" +
                        "Actual: " + masterCardNewBalance + "\n\n\n\n"
        );
    }
}
