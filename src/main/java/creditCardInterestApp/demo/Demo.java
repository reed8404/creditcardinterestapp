package creditCardInterestApp.demo;

import creditCardInterestApp.demo.demoCase.demoCaseImpl.CaseOne;
import creditCardInterestApp.demo.demoCase.demoCaseImpl.CaseThree;
import creditCardInterestApp.demo.demoCase.demoCaseImpl.CaseTwo;

/**
 * Created by ReedW on 12/20/2015.
 */
public class Demo {

    public static void main(String[] args) {
        CaseOne caseOne = new CaseOne();
        caseOne.displayCaseSpecs();
        caseOne.displayCaseCalculations();

        CaseTwo caseTwo = new CaseTwo();
        caseTwo.displayCaseSpecs();
        caseTwo.displayCaseCalculations();

        CaseThree caseThree = new CaseThree();
        caseThree.displayCaseSpecs();
        caseThree.displayCaseCalculations();
    }
}
