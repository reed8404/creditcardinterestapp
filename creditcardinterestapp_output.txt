CaseOne: visa
100.0
10.0
110.0


CaseOne: master card
100.0
5.0
105.0


CaseOne: discover
100.0
1.0
101.0


CaseOne: wallet
300.0
3.0
303.0


CaseOne: person
300.0
3.0
303.0


CaseOne: account
300.0
3.0
303.0

Test Case One:

This account has 1 person.

This person has 1 wallet.

This wallet has 3 cards:
1 Visa at 10% interest, 1 Master Card at 5% interest, 1 Discover at 1% interest

Each Card has a balance of $100.

Calculate the:
original balance, total interest(simple interest), and new balance including interest
For:
This account, its people, their wallets, and the cards inside of each wallet.


Calculations:

The original balance on this account:
Expected: 300.0
Actual: 300.0

The simple interest on this account:
Expected: 16.0
Actual: 3.0

The new balance of this account:
Expected: 316.0
Actual: 303.0

The original balance for this person:
Expected: 300.0
Actual: 300.0

The simple interest for this person:
Expected: 16.0
Actual: 3.0

The new balance for this person:
Expected: 316.0
Actual: 303.0

The original balance for this wallet:
Expected: 300.0
Actual: 300.0

The simple interest for this wallet:
Expected: 16.0
Actual: 3.0

The new balance for this wallet:
Expected: 316.0
Actual: 303.0

The original balance for this visa:
Expected: 100.0
Actual: 100.0

The simple interest for this visa:
Expected: 10.0
Actual: 10.0

The new balance for this visa:
Expected: 110.0
Actual: 110.0

The original balance for this Master Card:
Expected: 100.0
Actual: 100.0

The simple interest for this Master Card:
Expected: 5.0
Actual: 5.0

The new balance for this Master Card:
Expected: 105.0
Actual: 105.0

The original balance for this Discover:
Expected: 100.0
Actual: 100.0

The simple interest for this Discover:
Expected: 1.0
Actual: 1.0

The new balance for this Discover:
Expected: 101.0
Actual: 101.0




Test Case Two:

This account has 1 person.

This person has 2 wallets.

Wallet 1 has 2 cards:
1 Visa at 10% interest, 1 Discover at 1% interest

Wallet 2 has 1 card:
1 Master Card at 5% interest

Each card has a $100 balance.

Calculate the:
original balance, total interest(simple interest), and new balance including interest
For:
This account, its people, their wallets, and the cards inside of each wallet.


Calculations:

The original balance on this account:
Expected: 300.0
Actual: 300.0

The simple interest on this account:
Expected: 16.0
Actual: 3.0

The new balance of this account:
Expected: 316.0
Actual: 303.0

The original balance for this person:
Expected: 300.0
Actual: 300.0

The simple interest for this person:
Expected: 16.0
Actual: 3.0

The new balance for this person:
Expected: 316.0
Actual: 303.0

The original balance for the first wallet:
Expected: 200.0
Actual: 100.0

The simple interest for the first wallet:
Expected: 11.0
Actual: 1.0

The new balance for the first wallet:
Expected: 211.0
Actual: 101.0

The original balance for the visa in the first wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the visa in the first wallet:
Expected: 10.0
Actual: 10.0

The new balance for the visa in the first wallet:
Expected: 110.0
Actual: 110.0

The original balance for the Discover in the first wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the Discover in the first wallet:
Expected: 1.0
Actual: 1.0

The new balance for the Discover in the first wallet:
Expected: 101.0
Actual: 101.0

The original balance for the second wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the second wallet:
Expected: 5.0
Actual: 1.0

The new balance for the second wallet:
Expected: 105.0
Actual: 101.0

The original balance for the Master Card in the second wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the Master Card in the second wallet:
Expected: 5.0
Actual: 5.0

The new balance for the Master Card in the second wallet:
Expected: 105.0
Actual: 105.0




Test Case Three:

This account has 2 people.

Person 1 has 1 wallets.

This wallet has 2 cards:
1 Visa at 10% interest, 1 Master Card at 5% interest

Person 2 has 1 wallet.

This wallet also has 2 cards:
1 Visa at 10% interest, 1 Master Card at 5% interest

Each card has a $100 balance.

Calculate the:
original balance, total interest(simple interest), and new balance including interest
For:
This account, its people, their wallets, and the cards inside of each wallet.


Calculations:

The original balance on this account:
Expected: 400.0
Actual: 200.0

The simple interest on this account:
Expected: 30.0
Actual: 10.0

The new balance of this account:
Expected: 430.0
Actual: 210.0

The original balance for the first person:
Expected: 200.0
Actual: 200.0

The simple interest for the first person:
Expected: 15.0
Actual: 10.0

The new balance for the first person:
Expected: 215.0
Actual: 210.0

The original balance for the first person's wallet:
Expected: 200.0
Actual: 200.0

The simple interest for the first persons's wallet:
Expected: 15.0
Actual: 10.0

The new balance for the first person's wallet:
Expected: 215.0
Actual: 210.0

The original balance for the visa in the first wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the visa in the first wallet:
Expected: 10.0
Actual: 10.0

The new balance for the visa in the first wallet:
Expected: 110.0
Actual: 110.0

The original balance for the Master Card in the first wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the Master Card in the first wallet:
Expected: 5.0
Actual: 5.0

The new balance for the Master Card in the first wallet:
Expected: 105.0
Actual: 105.0

The original balance for the second person:
Expected: 200.0
Actual: 200.0

The simple interest for the second person:
Expected: 15.0
Actual: 10.0

The new balance for the second person:
Expected: 215.0
Actual: 210.0

The original balance for the second person's wallet:
Expected: 200.0
Actual: 200.0

The simple interest for the second person's wallet:
Expected: 15.0
Actual: 10.0

The new balance for the second person's wallet:
Expected: 215.0
Actual: 210.0

The original balance for the visa in the second wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the visa in the second wallet:
Expected: 10.0
Actual: 10.0

The new balance for the visa in the second wallet:
Expected: 110.0
Actual: 110.0

The original balance for the Master Card in the second wallet:
Expected: 100.0
Actual: 100.0

The simple interest for the Master Card in the second wallet:
Expected: 5.0
Actual: 5.0

The new balance for the Master Card in the second wallet:
Expected: 105.0
Actual: 105.0





Process finished with exit code 0